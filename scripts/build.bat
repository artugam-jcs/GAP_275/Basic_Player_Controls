@echo off

xcopy /Y "..\lib\SDL\lib\x64\SDL2.dll" "..\x64\Debug\SDL2.dll*"
xcopy /Y "..\lib\SDL\lib\x86\SDL2.dll" "..\Debug\SDL2.dll*"
xcopy /Y "..\lib\SDL\lib\x64\SDL2.dll" "..\x64\Release\SDL2.dll*"
xcopy /Y "..\lib\SDL\lib\x86\SDL2.dll" "..\Release\SDL2.dll*"

exit
