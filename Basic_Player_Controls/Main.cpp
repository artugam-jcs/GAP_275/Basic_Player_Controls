#include <iostream>
#include <string>

#include "App.h"

/**
 * @brief Program entry
 */
int main(int argc, char* argv[])
{
    auto app = App();

    int error = app.Init();

    if (error != 0)
        return error;

    app.Run();

    return 0;

}
