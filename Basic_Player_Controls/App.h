#pragma once

#include <vector>

// XXX: Include for class Color; use forward declaration after extraction.
#include "Color.h"

class SDL_Window;
class SDL_Renderer;
class GameObject;

class App
{
private:
	bool m_quit;

	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;

	std::vector<GameObject*> m_gameObjects;

	Color m_bgColor;  // Default is black.

	double m_deltaTime;

public:
	App();
	~App();

	int Init();
	void Run();
	void Update(double deltaTime);
	void Draw();

private:
	void GameInit();
};

