#pragma once

#include <vector>

template <class T >
inline void Delete(std::vector<T*> vec)
{
	for (int index = 0; index < vec.size(); ++index)
		delete vec.at(index);
}
