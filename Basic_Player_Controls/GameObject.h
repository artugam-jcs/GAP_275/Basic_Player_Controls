#pragma once

class SDL_Renderer;

#include "Vector2.h"

class GameObject
{
public:
	Vector2 position;

	virtual void Update(double deltaTime) = 0;
	virtual void Render(SDL_Renderer* pRenderer) = 0;
};

