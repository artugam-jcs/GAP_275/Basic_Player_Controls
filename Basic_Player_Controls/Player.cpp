#include "Player.h"

#include "Input.h"
#include "Color.h"

Player::Player()
{
	m_pRect = new SDL_Rect();

	m_pRect->w = 50;
	m_pRect->h = 50;

	m_color.r = 0;
	m_color.g = 255;
	m_color.b = 0;
}

Player::~Player()
{
	delete m_pRect;
}

void Player::Update(double deltaTime)
{
	auto input = Input::GetInstance();

#if _DEBUG
	// Testing input on the same frame!
	if (input->GetKeyUp(SDLK_d))
		std::cout << this->position.x << std::endl;

	if (input->GetKeyUp(SDLK_d))
		std::cout << this->position.y << std::endl;

	if (input->GetMouseDown())
		std::cout << "mouse1" << std::endl;

	if (input->GetMouseDown())
		std::cout << "mouse2" << std::endl;
#endif

	if (input->GetKey(SDLK_d))
		m_velocity.x = kSpeed;
	else if (input->GetKey(SDLK_a))
		m_velocity.x = -kSpeed;
	else
		m_velocity.x = 0;

	if (input->GetKey(SDLK_w))
		m_velocity.y = -kSpeed;
	else if (input->GetKey(SDLK_s))
		m_velocity.y = kSpeed;
	else
		m_velocity.y = 0;

	this->position.x += m_velocity.x * deltaTime;
	this->position.y += m_velocity.y * deltaTime;

	m_pRect->x = this->position.x;
	m_pRect->y = this->position.y;
}


void Player::Render(SDL_Renderer * pRenderer)
{
	SDL_SetRenderDrawColor(pRenderer, m_color.r, m_color.g, m_color.b, SDL_ALPHA_OPAQUE);

	SDL_RenderFillRect(pRenderer, m_pRect);
}