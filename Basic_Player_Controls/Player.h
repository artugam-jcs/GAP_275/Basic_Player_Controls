#pragma once

#include "Color.h"
#include "GameObject.h"

#include <iostream>
#include <SDL.h>

class Player : public GameObject
{
private:
	SDL_Rect* m_pRect;
	Color m_color;

	static constexpr float kSpeed = 500.0f;

	Vector2 m_velocity;

public:
	Player();
	~Player();

	virtual void Update(double deltaTime) override;
	virtual void Render(SDL_Renderer* pRenderer) override;
};

