#include <iostream>
#include <chrono>

#include <SDL.h>

#include "App.h"
#include "Player.h"
#include "Util.h"
#include "Input.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

static int Random(int min, int max)
{
	return rand() % max + min;
}

App::App()
	: m_quit(false)
	, m_pWindow(nullptr)
	, m_pRenderer(nullptr)
	, m_gameObjects(NULL)
	, m_deltaTime(0)
{

}

App::~App()
{
	Delete(m_gameObjects);

	SDL_Quit();
}

int App::Init()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		const char* error = SDL_GetError();
		std::cout << "Failed to initialize SDL." << std::endl;
		std::cout << error << std::endl;
		return 1;
	}

	std::cout << "Successfully initialized SDL!" << std::endl;

	m_pWindow = SDL_CreateWindow("Randomized_Mosaic",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH, SCREEN_HEIGHT,
		SDL_WINDOW_SHOWN);

	if (m_pWindow == nullptr)
	{
		std::cout << "Failed to create window. Error: " << SDL_GetError();
		return 2;
	}

	// Create Renderer.
	m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);

	if (m_pRenderer == nullptr)
	{
		std::cout << "Failed to create renderer. Error: " << SDL_GetError();
		SDL_DestroyWindow(m_pWindow);
		return 3;
	}

	srand(time(NULL));

	GameInit();

	return 0;
}

void App::GameInit()
{
	m_gameObjects.emplace_back(new Player());
}

void App::Run()
{
	auto lastFrameTime = std::chrono::high_resolution_clock::now();

	while (!this->m_quit)
	{
		auto thisFrameTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> lastFrameDuration = thisFrameTime - lastFrameTime;
		m_deltaTime = lastFrameDuration.count();
		lastFrameTime = thisFrameTime;

		auto input = Input::GetInstance();

		m_quit = !input->ProcessEvents();

		/*if (input->GetKey(SDLK_LCTRL) && input->GetKey(SDLK_q))
			m_quit = true;*/

		Update(m_deltaTime);
		Draw();
	}
}

void App::Update(double deltaTime)
{
	/* Update logic! */
	for (int index = 0; index < m_gameObjects.size(); ++index)
	{
		auto pGameObject = m_gameObjects.at(index);
		pGameObject->Update(deltaTime);
	}
}

void App::Draw()
{
	SDL_SetRenderDrawColor(m_pRenderer, m_bgColor.r, m_bgColor.g, m_bgColor.b, SDL_ALPHA_OPAQUE);

	// XXX: Should this be here?
	SDL_SetRenderDrawBlendMode(m_pRenderer, SDL_BLENDMODE_BLEND);

	SDL_RenderClear(m_pRenderer);

	/* Draw logic! */
	for (int index = 0; index < m_gameObjects.size(); ++index)
	{
		auto pGameObject = m_gameObjects.at(index);
		pGameObject->Render(m_pRenderer);
	}

	SDL_RenderPresent(m_pRenderer);
}
